package cellular;

import java.util.Random;

import datastructure.IGrid;
import datastructure.CellGrid;

public class BriansBrain implements CellAutomaton {

    /**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Brian's Brain Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		// TODO
		return this.currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		// TODO
		return this.currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		// TODO
		return this.currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		// TODO
		for (int i = 0; i < this.numberOfRows(); i++) {
			for (int j = 0; j < this.numberOfColumns(); j++) {
				nextGeneration.set(i, j, getNextCell(i, j));
			}
		}
        this.currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		// TODO
		CellState stateCell = getCellState(row, col);
		if (stateCell.equals(CellState.ALIVE)) {
			return CellState.DYING;
		} else if (stateCell.equals(CellState.DEAD) && countNeighbors(row, col, CellState.ALIVE) == 2) {
			return CellState.ALIVE;
		}
		return CellState.DEAD;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		// TODO
		int neighbourCounter = 0;
		if (getCellState(row, col).equals(state)) neighbourCounter--;
		for (int i = row-1; i <= row+1; i++) {
			if (row - 1 < 0 || row + 1 > numberOfRows()-1) continue;
			for (int j = col-1; j <= col+1; j++) {
				if (col - 1 < 0 || col + 1 > numberOfColumns()-1) continue;
				if (getCellState(i, j).equals(state)) neighbourCounter++;
			}
		}
		return neighbourCounter;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
