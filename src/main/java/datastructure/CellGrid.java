package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        if (rows < 0 || columns < 0) {
            throw new IndexOutOfBoundsException();
        } else {
            this.rows = rows;
            this.cols = columns;
            this.grid = new CellState[rows][cols];
            for (int i = 0; i < this.rows; i++) {
                for (int j = 0; j < this.cols; j++) {
                    this.grid[i][j] = initialState;
                }
            }
        }
        
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if (!(0 <= row && row <= this.rows) || !(0 <= column && column <= this.cols)) {
            throw new IndexOutOfBoundsException();
        } else {
            this.grid[row][column] = element;
        }
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if (!(0 <= row && row <= this.rows) || !(0 <= column && column <= this.cols)) {
            throw new IndexOutOfBoundsException();
        } else {
            return this.grid[row][column];
        }
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        IGrid copiedGrid = new CellGrid(this.rows, this.cols, (this.get(0, 0)));
        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.cols; j++) {
                copiedGrid.set(i, j, this.get(i, j));
            }
        }
        return copiedGrid;
    }
    
}
